// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

interface IONFT721Psi {
    function mint(address owner, uint256 quantity, bytes32[] memory merkleProof) external;
    function maxSupply() external view returns (uint256);
    function mintPrice() external view returns (uint256);
    function creator() external view returns (address);
    function createdAt() external view returns (uint256);
    function dropFrom() external view returns (uint256);
}
