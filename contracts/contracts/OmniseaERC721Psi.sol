// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../interfaces/IONFT721Psi.sol";
import "../interfaces/IERC2981Royalties.sol";
import {CreateParams, Allowlist} from "../structs/erc721/ERC721Structs.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "./ERC721Psi/ERC721Psi.sol";
import "../onft/ONFT721Core.sol";

contract OmniseaERC721Psi is ONFT721Core, IONFT721Psi, ERC721Psi, ReentrancyGuard {
    using Strings for uint256;

    event TokenMinted(address collAddr, address owner, uint256 tokenId);

    struct RoyaltyInfo {
        address recipient;
        uint24 amount;
    }

    modifier onlyTokenFactory() {
        require(msg.sender == tokenFactory);
        _;
    }

    address private constant TREASURY = 0x61104fBe07ecc735D8d84422c7f045f8d29DBf15;

    string public collectionName;
    uint256 public override createdAt;
    uint256 public override maxSupply;
    address public override creator;
    uint256 public override dropFrom;
    uint256 public dropTo;
    string public collectionURI;
    uint256 public publicPrice;
    address public tokenFactory;
    string public tokensURI;
    mapping(uint256 => string) public tokenURIMap;
    uint256 public mintLimit;
    uint256[] public tokenURIIndexFrom;
    string public _notRevealedURI;
    Allowlist public allowlist;
    mapping(address => uint256) mintedCount;
    mapping(address => uint256) allowlistMintedCount;
    bool public isZeroIndexed;
    uint256 private preMintedToPlatform;
    uint256 private preMintedToTeam;
    RoyaltyInfo private _royalties;

    constructor(
        string memory _symbol,
        CreateParams memory params,
        address _creator,
        address _tokenFactoryAddress
    ) ERC721Psi(params.name, _symbol) ONFT721Core(250000, address(0x9b896c0e23220469C7AE69cb4BbAE391eAa4C8da)) {
        tokenFactory = _tokenFactoryAddress;
        creator = _creator;
        tokensURI = params.tokensURI;
        maxSupply = params.maxSupply;
        publicPrice = params.price;
        createdAt = block.timestamp;
        collectionName = params.name;
        collectionURI = params.uri;
        isZeroIndexed = params.isZeroIndexed;
        _setDates(params.from, params.to);
        _setNextTokenId(isZeroIndexed ? 0 : 1);
        _royalties = RoyaltyInfo(creator, params.royaltyAmount);
        _transferOwnership(_creator);
    }

    function _baseURI() internal pure returns (string memory) {
        return "ipfs://";
    }

    function contractURI() public view returns (string memory) {
        return string(abi.encodePacked(_baseURI(), collectionURI));
    }

    function tokenURI(uint256 tokenId) public view returns (string memory) {
        if (bytes(_notRevealedURI).length > 0) {
            return _notRevealedURI;
        }

        return string(abi.encodePacked(_baseURI(), _getTokenURI(tokenId), "/", tokenId.toString(), ".json"));
    }

    function mint(address _owner, uint256 _quantity, bytes32[] memory _merkleProof) override external nonReentrant {
        _validateMint(_quantity, _owner, _merkleProof);
        _safeMint(_owner, _quantity);
        emit TokenMinted(address(this), _owner, (_nextTokenId() - 1));
    }

    function _validateMint(uint256 _quantity, address _owner, bytes32[] memory _merkleProof) internal onlyTokenFactory {
        uint256 _newTotalMinted = totalMinted() + _quantity;
        if (maxSupply > 0) require(maxSupply >= _newTotalMinted, ">maxSupply");
        if (dropFrom > 0) require(block.timestamp >= dropFrom, "!started");
        if (dropTo > 0) require(block.timestamp <= dropTo, "ended");
        if (mintLimit > 0) require(mintLimit >= _newTotalMinted, ">mintLimit");

        mintedCount[_owner] += _quantity;
        if (allowlist.isEnabled) {
            if (block.timestamp >= allowlist.publicFrom) {
                uint256 publicMints = mintedCount[_owner] - allowlistMintedCount[_owner];

                require(allowlist.maxPerAddressPublic >= publicMints, ">maxPerAddressPublic");
            } else {
                require(isAllowlisted(_owner, _merkleProof), "!allowlisted");
                allowlistMintedCount[_owner] += _quantity;
                require(allowlist.maxPerAddress >= allowlistMintedCount[_owner], ">maxPerAddress");
            }
        }
    }

    function _setDates(uint256 from, uint256 to) internal {
        if (from > 0) {
            require(from >= (block.timestamp - 1 days));
            dropFrom = from;
        }
        if (to > 0) {
            require(to > from && to > block.timestamp);
            dropTo = to;
        }
    }

    function setNotRevealedURI(string memory _uri) external onlyOwner {
        require(totalMinted() == preMintedToPlatform + preMintedToTeam);
        _notRevealedURI = _uri;
    }

    function reveal() external {
        require(msg.sender == owner() || msg.sender == TREASURY);
        _notRevealedURI = "";
    }

    function mintPrice() public view override returns (uint256) {
        if (allowlist.isEnabled && block.timestamp < allowlist.publicFrom) {
            return allowlist.price;
        }

        return publicPrice;
    }

    function isAllowlisted(address _account, bytes32[] memory _merkleProof) public view returns (bool) {
        bytes32 leaf = keccak256(abi.encodePacked(_account));

        return MerkleProof.verify(_merkleProof, allowlist.merkleRoot, leaf);
    }

    function setAllowlist(bytes32 merkleRoot, uint256 maxPerAddress, uint256 maxPerAddressPublic, uint256 publicFrom, uint256 price, bool isEnabled) external onlyOwner {
        allowlist = Allowlist(maxPerAddress, maxPerAddressPublic, publicFrom, price, merkleRoot, isEnabled);
    }

    function toggleAllowlist(bool _isEnabled) external onlyOwner {
        allowlist.isEnabled = _isEnabled;
    }

    function preMintToTeam(uint256 _quantity) external onlyOwner {
        if (dropFrom > 0) {
            require(block.timestamp < dropFrom, ">= dropFrom");
        }
        preMintedToTeam += _quantity;
        _mint(creator, _quantity);
    }

//    function preMintToPlatform() external {
//        require(preMintedToPlatform == 0, "isPreMinted");
//        uint256 _quantity = maxSupply < 10000 ? (maxSupply < 100 ? 1 : 3) : 5;
//        preMintedToPlatform = _quantity;
//        _mint(TREASURY, _quantity);
//    }

    function setNextTokenURI(uint256 _fromTokenId, string memory _nextTokenURI, uint256 _mintLimit) external onlyOwner {
        require(_fromTokenId <= maxSupply, "from>maxSupply");
        require(_mintLimit <= maxSupply, "to>maxSupply");

        if (tokenURIIndexFrom.length > 0) {
            require(tokenURIIndexFrom[tokenURIIndexFrom.length - 1] < _fromTokenId, "!new");
        }

        tokenURIIndexFrom.push(_fromTokenId);
        uint256 tokenURIsCount = tokenURIIndexFrom.length;
        tokenURIMap[tokenURIsCount] = _nextTokenURI;
        mintLimit = _mintLimit;
    }

    function _getTokenURI(uint256 tokenId) internal view returns (string memory) {
        uint256 tokenURIsCount = tokenURIIndexFrom.length;

        if (tokenURIsCount == 0) {
            return tokensURI;
        }

        for (uint256 i = tokenURIsCount; i >= 1; i--) {
            if (tokenId >= tokenURIIndexFrom[i - 1]) {
                return tokenURIMap[i];
            }
        }

        return tokensURI;
    }

    function _startTokenId() internal view override returns (uint256) {
        return isZeroIndexed ? 0 : 1;
    }

    function royaltyInfo(uint256, uint256 value) external view returns (address receiver, uint256 royaltyAmount) {
        RoyaltyInfo memory royalties = _royalties;
        receiver = royalties.recipient;
        royaltyAmount = (value * royalties.amount) / 10000;
    }

    function setRoyaltyAmount(uint24 _amount) external onlyOwner {
        _royalties.amount = _amount;
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ONFT721Core, ERC721Psi) returns (bool) {
        return interfaceId == type(IONFT721Psi).interfaceId
        || interfaceId == type(IERC2981Royalties).interfaceId
        || super.supportsInterface(interfaceId);
    }

    function _debitFrom(address _from, uint16, bytes memory, uint _tokenId) internal virtual override {
        require(ownerOf(_tokenId) == _from);
        require(_isApprovedOrOwner(_from, _tokenId));
        transferFrom(_from, address(this), _tokenId);
    }

    function _creditTo(uint16, address _toAddress, uint _tokenId) internal virtual override {
        require(_exists(_tokenId) && ownerOf(_tokenId) == address(this));
        transferFrom(address(this), _toAddress, _tokenId);
    }
}
