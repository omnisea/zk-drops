// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "../interfaces/IONFT721Psi.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { MintParams } from "../structs/erc721/ERC721Structs.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract TokenFactory is Ownable, ReentrancyGuard {
    event Minted(address collAddr, address rec, uint256 quantity);

    uint256 private _fee;
    address private _feeManager;

    constructor() {
        _feeManager = address(0x61104fBe07ecc735D8d84422c7f045f8d29DBf15);
    }

    function setFee(uint256 fee) external onlyOwner {
        require(fee <= 5);
        _fee = fee;
    }

    function setFeeManager(address _newManager) external onlyOwner {
        _feeManager = _newManager;
    }

    function mintToken(MintParams calldata params) public payable nonReentrant {
        require(params.quantity > 0, "!quantity");
        require(params.coll != address(0));
        IONFT721Psi collection = IONFT721Psi(params.coll);

        uint256 price = collection.mintPrice();
        uint256 quantityPrice = price * params.quantity;
        if (price > 0) {
            require(msg.value == quantityPrice, "<price");

            (bool p1,) = payable(collection.creator()).call{value: (msg.value * (100 - _fee) / 100)}("");
            require(p1, "!p1");

            if (_fee > 0) {
                (bool p2,) = payable(_feeManager).call{value: (msg.value * _fee / 100)}("");
                require(p2, "!p2");
            }
        }

        collection.mint(msg.sender, params.quantity, params.merkleProof);
        emit Minted(params.coll, msg.sender, params.quantity);
    }

    receive() external payable {}
}
